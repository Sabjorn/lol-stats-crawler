﻿namespace LoLCrawler.Data;

public readonly struct Match
{
    public Participant[] BlueTeamChampions { get; init; }
    public Participant[] RedTeamChampions { get; init; }
    public Team WinTeam { get; init; }
    public int MatchHash { get; init; }

    private Match(
        Participant[] BlueTeamChampions,
        Participant[] RedTeamChampions,
        Team WinTeam,
        int MatchHash
    )
    {
        this.BlueTeamChampions = BlueTeamChampions;
        this.RedTeamChampions = RedTeamChampions;
        this.WinTeam = WinTeam;
        this.MatchHash = MatchHash;
    }

    public static Match Create(
        Participant[] BlueTeamChampions,
        Participant[] RedTeamChampions,
        Team WinTeam
    )
    {
        var hash = CreateHash(BlueTeamChampions, RedTeamChampions, WinTeam);
        return new Match(BlueTeamChampions, RedTeamChampions, WinTeam, hash);
    }

    private static int CreateHash(
        Participant[] BlueTeamChampions,
        Participant[] RedTeamChampions,
        Team WinTeam
    )
    {
        int hash = 0;

        foreach (var p in BlueTeamChampions)
        {
            hash ^= p.SummonerName.GetHashCode();
            hash ^= (int)p.Champion;
        }

        foreach (var p in RedTeamChampions)
        {
            hash ^= p.SummonerName.GetHashCode();
            hash ^= (int)p.Champion;
        }

        if (WinTeam == Team.Red) hash = ~hash;

        return hash;
    }
}

public readonly record struct Participant(string SummonerName, Champion Champion);