﻿namespace LoLCrawler.Utility;

public static class HashSetExtensions
{
    public static void AddRange<T>(this HashSet<T> self, IEnumerable<T> sources)
    {
        foreach (var e in sources)
        {
            self.Add(e);
        }
    }
    public static void AddRange<T>(this Queue<T> self, IEnumerable<T> sources)
    {
        foreach (var e in sources)
        {
            self.Enqueue(e);
        }
    }
}