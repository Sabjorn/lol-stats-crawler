﻿using LoLCrawler.Agency;

const string snapshotPath = "snapshot.json";

var agency = new Agency();
Console.WriteLine("Agency established.");

if (File.Exists(snapshotPath))
{
    Console.WriteLine("Snapshot found. Importing...");
    
    agency.ImportSnapshot(snapshotPath);
    
    Console.WriteLine($"{agency.NumMatches} matches has been imported.");
}
else
{
    Console.WriteLine("Snapshot not found. Seeding summoner by chikuwa3.");
    
    agency.EnqueueSummoner("chikuwa3");
}

var cancellationToken = new CancellationTokenSource();

void OnCancelKeyPressed(object? sender, ConsoleCancelEventArgs eventArgs)
{
    eventArgs.Cancel = true;
    cancellationToken.Cancel();
}

Console.CancelKeyPress += OnCancelKeyPressed;

try
{
    await agency.RunAsync(cancellationToken.Token);
}
catch (Exception e)
{
    Console.WriteLine(!cancellationToken.IsCancellationRequested
        ? $"Operation has aborted unexpectedly:\n{e}"
        : "Operation has cancelled."
    );
}

Console.WriteLine("Exporting snapshot...");

var jsonString = agency.ExportProgressJson();

var file = File.OpenWrite("snapshot.json");
StreamWriter writer = new StreamWriter(file);
await writer.WriteAsync(jsonString);
await writer.DisposeAsync();
await file.DisposeAsync();

Console.WriteLine("Done.");