﻿using System.Text.Json;

namespace LoLCrawler.Apis.Mobalytics;

public static class MobalyticsApi
{
    private const string ApiUrl = "https://app.mobalytics.gg/api/lol/graphql/v1/query";

    private static string GetSummonerInfoBody(string summonerName) => $@"
{{
    ""operationName"": ""LolProfilePageSummonerOverviewQuery"",
    ""variables"": {{
        ""top"": 1000,
        ""withMatchParticipantDetailed"": true,
        ""summonerName"": ""{summonerName}"",
        ""region"": ""JP"",
        ""skip"": 0
    }},
    ""extensions"": {{
        ""persistedQuery"": {{
            ""version"": 1,
            ""sha256Hash"": ""b40d1611a6d63e0750102f3860585d87fda084d15f083b1948e45056522ed724""
        }}
    }}
}}
";

    public static async Task<SummonerOverviewQueryResponse?> GetSummonerInfo(string summonerName, CancellationToken ct)
    {
        var response = await HttpMethod.PostJson<SummonerOverviewQueryResponse>(ApiUrl, GetSummonerInfoBody(summonerName), ct);

        return response;
    }
}