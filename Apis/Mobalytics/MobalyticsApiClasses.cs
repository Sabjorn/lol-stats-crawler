﻿using System.Text.Json.Serialization;

namespace LoLCrawler.Apis.Mobalytics;

// Root myDeserializedClass = JsonSerializer.Deserialize<Root>(myJsonResponse);
public record Actual(
    [property: JsonPropertyName("seasonId")]
    object SeasonId,
    [property: JsonPropertyName("queue")] string Queue,
    [property: JsonPropertyName("rolename")]
    object Rolename,
    [property: JsonPropertyName("championId")]
    object ChampionId,
    [property: JsonPropertyName("__typename")]
    string Typename
);

public record AvgTier(
    [property: JsonPropertyName("tier")] string Tier,
    [property: JsonPropertyName("division")]
    string Division,
    [property: JsonPropertyName("__typename")]
    string Typename
);

public record Badge(
    [property: JsonPropertyName("slug")] string Slug,
    [property: JsonPropertyName("values")] IReadOnlyList<double> Values,
    [property: JsonPropertyName("type")] string Type,
    [property: JsonPropertyName("__typename")]
    string Typename
);

public record Build(
    [property: JsonPropertyName("id")] string Id,
    [property: JsonPropertyName("type")] string Type,
    [property: JsonPropertyName("spells")] IReadOnlyList<int> Spells,
    [property: JsonPropertyName("perks")] Perks Perks,
    [property: JsonPropertyName("__typename")]
    string Typename
);

public record Data(
    [property: JsonPropertyName("lol")] Lol Lol
);

public record Filters(
    [property: JsonPropertyName("actual")] Actual Actual,
    [property: JsonPropertyName("options")]
    Options Options,
    [property: JsonPropertyName("__typename")]
    string Typename
);

public record Item(
    [property: JsonPropertyName("role")] string Role,
    [property: JsonPropertyName("won")] int Won,
    [property: JsonPropertyName("lost")] int Lost,
    [property: JsonPropertyName("__typename")]
    string Typename,
    [property: JsonPropertyName("timestamp")]
    int Timestamp,
    [property: JsonPropertyName("duration")]
    int Duration
);

public record Kda(
    [property: JsonPropertyName("k")] int K,
    [property: JsonPropertyName("d")] int D,
    [property: JsonPropertyName("a")] int A,
    [property: JsonPropertyName("__typename")]
    string Typename
);

public record LaneMatchup(
    [property: JsonPropertyName("championId")]
    int ChampionId,
    [property: JsonPropertyName("__typename")]
    string Typename
);

public record Lol(
    [property: JsonPropertyName("player")] Player Player,
    [property: JsonPropertyName("__typename")]
    string Typename
);

public record Lp(
    [property: JsonPropertyName("before")] object Before,
    [property: JsonPropertyName("after")] object After,
    [property: JsonPropertyName("lpDiff")] object LpDiff,
    [property: JsonPropertyName("promoted")]
    object Promoted,
    [property: JsonPropertyName("__typename")]
    string Typename
);

public record Match(
    [property: JsonPropertyName("id")] int Id,
    [property: JsonPropertyName("matchId")]
    int MatchId,
    [property: JsonPropertyName("seasonId")]
    string SeasonId,
    [property: JsonPropertyName("queue")] string Queue,
    [property: JsonPropertyName("startedAt")]
    int StartedAt,
    [property: JsonPropertyName("duration")]
    int Duration,
    [property: JsonPropertyName("patch")] string Patch,
    [property: JsonPropertyName("teams")] IReadOnlyList<Team> Teams,
    [property: JsonPropertyName("subject")]
    Subject Subject,
    [property: JsonPropertyName("participants")]
    IReadOnlyList<Participant> Participants,
    [property: JsonPropertyName("avgTier")]
    AvgTier AvgTier,
    [property: JsonPropertyName("laneMatchup")]
    LaneMatchup LaneMatchup,
    [property: JsonPropertyName("__typename")]
    string Typename
);

public record MatchesActivity(
    [property: JsonPropertyName("items")] IReadOnlyList<Item> Items,
    [property: JsonPropertyName("__typename")]
    string Typename
);

public record MatchesHistory(
    [property: JsonPropertyName("hasNext")]
    bool HasNext,
    [property: JsonPropertyName("matches")]
    IReadOnlyList<Match> Matches,
    [property: JsonPropertyName("filters")]
    Filters Filters,
    [property: JsonPropertyName("__typename")]
    string Typename
);

public record Options(
    [property: JsonPropertyName("champions")]
    IReadOnlyList<int> Champions,
    [property: JsonPropertyName("queues")] IReadOnlyList<string> Queues,
    [property: JsonPropertyName("rolenames")]
    object Rolenames,
    [property: JsonPropertyName("seasonIds")]
    object SeasonIds,
    [property: JsonPropertyName("__typename")]
    string Typename
);

public record Participant(
    [property: JsonPropertyName("role")] string Role,
    [property: JsonPropertyName("region")] string Region,
    [property: JsonPropertyName("summonerName")]
    string SummonerName,
    [property: JsonPropertyName("championId")]
    int ChampionId,
    [property: JsonPropertyName("championLevel")]
    int ChampionLevel,
    [property: JsonPropertyName("team")] string Team,
    [property: JsonPropertyName("__typename")]
    string Typename,
    [property: JsonPropertyName("mvpScore")]
    int MvpScore,
    [property: JsonPropertyName("cs")] int Cs,
    [property: JsonPropertyName("csDiff10")]
    int CsDiff10,
    [property: JsonPropertyName("kp")] int Kp,
    [property: JsonPropertyName("controlWardsPlaced")]
    int ControlWardsPlaced,
    [property: JsonPropertyName("farsightWardsPlaced")]
    int FarsightWardsPlaced,
    [property: JsonPropertyName("sightWardsPlaced")]
    int SightWardsPlaced,
    [property: JsonPropertyName("wardsCleared")]
    int WardsCleared,
    [property: JsonPropertyName("wardsPlaced")]
    int WardsPlaced,
    [property: JsonPropertyName("gold")] int Gold,
    [property: JsonPropertyName("kda")] Kda Kda,
    [property: JsonPropertyName("finalBuild")]
    object FinalBuild,
    [property: JsonPropertyName("build")] Build Build,
    [property: JsonPropertyName("rank")] object Rank,
    [property: JsonPropertyName("damageToChampions")]
    int DamageToChampions,
    [property: JsonPropertyName("lp")] object Lp
);

public record Perks(
    [property: JsonPropertyName("IDs")] IReadOnlyList<int> IDs,
    [property: JsonPropertyName("style")] int Style,
    [property: JsonPropertyName("subStyle")]
    int SubStyle,
    [property: JsonPropertyName("__typename")]
    string Typename
);

public record Player(
    [property: JsonPropertyName("aId")] string AId,
    [property: JsonPropertyName("rolesDistribution")]
    RolesDistribution RolesDistribution,
    [property: JsonPropertyName("matchesActivity")]
    MatchesActivity MatchesActivity,
    [property: JsonPropertyName("teammatesActivity")]
    TeammatesActivity TeammatesActivity,
    [property: JsonPropertyName("matchesHistory")]
    MatchesHistory MatchesHistory,
    [property: JsonPropertyName("liveGame")]
    object LiveGame,
    [property: JsonPropertyName("__typename")]
    string Typename
);

public record RolesDistribution(
    [property: JsonPropertyName("items")] IReadOnlyList<Item> Items,
    [property: JsonPropertyName("__typename")]
    string Typename
);

public record SummonerOverviewQueryResponse(
    [property: JsonPropertyName("data")] Data Data
);

public record Subject(
    [property: JsonPropertyName("role")] string Role,
    [property: JsonPropertyName("region")] string Region,
    [property: JsonPropertyName("summonerName")]
    string SummonerName,
    [property: JsonPropertyName("championId")]
    int ChampionId,
    [property: JsonPropertyName("championLevel")]
    int ChampionLevel,
    [property: JsonPropertyName("team")] string Team,
    [property: JsonPropertyName("__typename")]
    string Typename,
    [property: JsonPropertyName("mvpScore")]
    int MvpScore,
    [property: JsonPropertyName("cs")] int Cs,
    [property: JsonPropertyName("csDiff10")]
    int CsDiff10,
    [property: JsonPropertyName("kp")] double Kp,
    [property: JsonPropertyName("controlWardsPlaced")]
    int ControlWardsPlaced,
    [property: JsonPropertyName("farsightWardsPlaced")]
    int FarsightWardsPlaced,
    [property: JsonPropertyName("sightWardsPlaced")]
    int SightWardsPlaced,
    [property: JsonPropertyName("wardsCleared")]
    int WardsCleared,
    [property: JsonPropertyName("wardsPlaced")]
    int WardsPlaced,
    [property: JsonPropertyName("gold")] int Gold,
    [property: JsonPropertyName("kda")] Kda Kda,
    [property: JsonPropertyName("finalBuild")]
    IReadOnlyList<int> FinalBuild,
    [property: JsonPropertyName("build")] Build Build,
    [property: JsonPropertyName("rank")] object Rank,
    [property: JsonPropertyName("damageToChampions")]
    int DamageToChampions,
    [property: JsonPropertyName("lp")] Lp Lp,
    [property: JsonPropertyName("badges")] IReadOnlyList<Badge> Badges
);

public record Team(
    [property: JsonPropertyName("teamId")] string TeamId,
    [property: JsonPropertyName("result")] string Result,
    [property: JsonPropertyName("avgTier")]
    AvgTier AvgTier,
    [property: JsonPropertyName("inhibitorKills")]
    int InhibitorKills,
    [property: JsonPropertyName("dragonKills")]
    object DragonKills,
    [property: JsonPropertyName("objectivesTaken")]
    object ObjectivesTaken,
    [property: JsonPropertyName("__typename")]
    string Typename
);

public record TeammatesActivity(
    [property: JsonPropertyName("items")] IReadOnlyList<object> Items,
    [property: JsonPropertyName("__typename")]
    string Typename
);