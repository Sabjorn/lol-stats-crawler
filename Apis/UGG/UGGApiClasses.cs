﻿using System.Text.Json.Serialization;

namespace LoLCrawler.Apis.UGG;

public record Data(
    [property: JsonPropertyName("fetchPlayerMatchSummaries")]
    FetchPlayerMatchSummaries FetchPlayerMatchSummaries
);

public record FetchPlayerMatchSummaries(
    [property: JsonPropertyName("matchSummaries")]
    IReadOnlyList<MatchSummary> MatchSummaries
);

public record MatchSummary(
    [property: JsonPropertyName("teamA")] IReadOnlyList<TeamA> TeamA,
    [property: JsonPropertyName("teamB")] IReadOnlyList<TeamB> TeamB,
    [property: JsonPropertyName("win")] bool Win
);

public record FetchMatchSummariesResponse(
    [property: JsonPropertyName("data")] Data Data
);

public record TeamA(
    [property: JsonPropertyName("championId")]
    int ChampionId,
    [property: JsonPropertyName("hardCarry")]
    double HardCarry,
    [property: JsonPropertyName("role")] int Role,
    [property: JsonPropertyName("summonerName")]
    string SummonerName,
    [property: JsonPropertyName("teamId")] object TeamId,
    [property: JsonPropertyName("teamplay")]
    double Teamplay
);

public record TeamB(
    [property: JsonPropertyName("championId")]
    int ChampionId,
    [property: JsonPropertyName("hardCarry")]
    double HardCarry,
    [property: JsonPropertyName("role")] int Role,
    [property: JsonPropertyName("summonerName")]
    string SummonerName,
    [property: JsonPropertyName("teamId")] object TeamId,
    [property: JsonPropertyName("teamplay")]
    double Teamplay
);