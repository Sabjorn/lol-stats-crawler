﻿namespace LoLCrawler.Apis.UGG;

public static class UGGApi
{
    private const string ApiUrl = "https://u.gg/api";

    private static string FetchMatchSummariesBody(string summonerName) => $@"
{{""query"":""query FetchMatchSummaries($championId: [Int], $page: Int, $queueType: [Int], $duoName: String, $regionId: String!, $role: [Int], $seasonIds: [Int]!, $summonerName: String!) {{\r\n fetchPlayerMatchSummaries(\r\n championId: $championId\r\n page: $page\r\n queueType: $queueType\r\n duoName: $duoName\r\n regionId: $regionId\r\n role: $role\r\n seasonIds: $seasonIds\r\n summonerName: $summonerName\r\n ) {{\r\n matchSummaries {{\r\n teamA {{\r\n championId\r\n summonerName\r\n teamId\r\n role\r\n hardCarry\r\n teamplay\r\n }}\r\n teamB {{\r\n championId\r\n summonerName\r\n teamId\r\n role\r\n hardCarry\r\n teamplay\r\n }}\r\n win\r\n }}\r\n }}\r\n}}\r\n"",""variables"":{{
""summonerName"":""{summonerName}"",
""regionId"":""jp1"",
""seasonIds"":[20]
}}}}
";

    public static async Task<FetchMatchSummariesResponse?> FetchMatchSummaries(string summonerName, CancellationToken ct)
    {
        var response =
            await HttpMethod.PostJson<FetchMatchSummariesResponse>(ApiUrl, FetchMatchSummariesBody(summonerName), ct);

        return response;
    }
}