﻿using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;

namespace LoLCrawler.Apis;

public class HttpMethod
{
    public static async Task<Stream> Get(string url, CancellationToken ct)
    {
        using var client = new HttpClient();

        return await client.GetStreamAsync(url, ct);
    }
    
    public static async Task<T?> GetJson<T>(string url ,CancellationToken ct)
    {
        var responseStream = await Get(url, ct);

        return await JsonSerializer.DeserializeAsync<T>(responseStream, cancellationToken: ct);
    }
    public static async Task<HttpResponseMessage> Post(string url, string content, CancellationToken ct, string contentType)
    {
        using var client = new HttpClient();

        var response = await client.PostAsync(url, new StringContent(content, Encoding.UTF8, contentType), ct);

        if (!response.IsSuccessStatusCode) throw new ApplicationException(await response.Content.ReadAsStringAsync(ct));
        
        return response;
    }
    
    public static async Task<T?> PostJson<T>(string url, string content, CancellationToken ct)
    {
        var responseStream = await Post(url, content, ct, "application/json");

        return await JsonSerializer.DeserializeAsync<T>(await responseStream.Content.ReadAsStreamAsync(ct), cancellationToken: ct);
    }
    
    public static async Task<JsonDocument> PostJson(string url, string content, CancellationToken ct)
    {
        var responseStream = await Post(url, content, ct, "application/json");

        var document =
            await JsonDocument.ParseAsync(await responseStream.Content.ReadAsStreamAsync(ct), cancellationToken: ct);

        return document;
    }
    
    public static async Task<JsonDocument> PostGraphQL(string url, string content, CancellationToken ct)
    {
        var responseStream = await Post(url, content, ct, "text/plain");

        var document =
            await JsonDocument.ParseAsync(await responseStream.Content.ReadAsStreamAsync(ct), cancellationToken: ct);

        return document;
    }
}