﻿using System.Net;
using System.Text.Json;
using AngleSharp.Dom;
using AngleSharp.Html.Parser;

namespace LoLCrawler.Apis.OPGG;

public static class OPGGApi
{
    private static string GetRenewalApiUrl(string summonerId)
        => $"https://op.gg/api/v1.0/internal/bypass/summoners/jp/{summonerId}/renewal";

    private static string GetRenewalStatusApiUrl(string summonerId)
        => $"https://op.gg/api/v1.0/internal/bypass/summoners/jp/{summonerId}/renewal-status";

    private static string GetSummonerApiUrl(string summonerId)
        => $"https://op.gg/api/v1.0/internal/bypass/games/jp/summoners/{summonerId}?&limit=20&hl=ja_JP&game_type=normal";

    private static string SummonerPageUrl(string summonerName) => $"https://www.op.gg/summoners/jp/{summonerName}";
    
    public static async Task<string?> GetSummonerId(string summonerName, CancellationToken ct)
    {
        var html = await HttpMethod.Get(SummonerPageUrl(summonerName), ct);

        var parser = new HtmlParser();
        var document = await parser.ParseDocumentAsync(html, ct);

        var propJsonString = document.QuerySelector("#__NEXT_DATA__")?.Text() ?? string.Empty;

        var jsonDocument = JsonDocument.Parse(propJsonString);

        var summonerId = jsonDocument.RootElement
            .GetProperty("props")
            .GetProperty("pageProps")
            .GetProperty("data")
            .GetProperty("summoner_id")
            .GetString();
        
        return summonerId;
    }

    public static async Task<SummonerApiResponse?> GetSummonerData(string summonerId, CancellationToken ct)
    {
        var jsonObject = await HttpMethod.GetJson<SummonerApiResponse>(GetSummonerApiUrl(summonerId), ct);

        return jsonObject;
    }
}