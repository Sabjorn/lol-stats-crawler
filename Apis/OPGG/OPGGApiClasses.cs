﻿using System.Text.Json.Serialization;

namespace LoLCrawler.Apis.OPGG;

// Root myDeserializedClass = JsonSerializer.Deserialize<Root>(myJsonResponse);
public class AverageTierInfo
{
    [JsonPropertyName("tier")] public string Tier { get; set; }

    [JsonPropertyName("division")] public int Division { get; set; }

    [JsonPropertyName("tier_image_url")] public string TierImageUrl { get; set; }

    [JsonPropertyName("border_image_url")] public string BorderImageUrl { get; set; }
}

public class Datum
{
    [JsonPropertyName("id")] public string Id { get; set; }

    [JsonPropertyName("created_at")] public DateTime CreatedAt { get; set; }

    [JsonPropertyName("game_map")] public string GameMap { get; set; }

    [JsonPropertyName("queue_info")] public QueueInfo QueueInfo { get; set; }

    [JsonPropertyName("version")] public string Version { get; set; }

    [JsonPropertyName("game_length_second")]
    public int GameLengthSecond { get; set; }

    [JsonPropertyName("is_remake")] public bool IsRemake { get; set; }

    [JsonPropertyName("is_opscore_active")]
    public bool IsOpscoreActive { get; set; }

    [JsonPropertyName("is_recorded")] public bool IsRecorded { get; set; }

    [JsonPropertyName("average_tier_info")]
    public AverageTierInfo AverageTierInfo { get; set; }

    [JsonPropertyName("participants")] public List<Participant> Participants { get; set; }

    [JsonPropertyName("teams")] public List<Team> Teams { get; set; }

    [JsonPropertyName("myData")] public MyData MyData { get; set; }
}

public class GameStat
{
    [JsonPropertyName("dragon_kill")] public int DragonKill { get; set; }

    [JsonPropertyName("baron_kill")] public int BaronKill { get; set; }

    [JsonPropertyName("tower_kill")] public int TowerKill { get; set; }

    [JsonPropertyName("is_win")] public bool IsWin { get; set; }

    [JsonPropertyName("is_remake")] public bool IsRemake { get; set; }

    [JsonPropertyName("kill")] public int Kill { get; set; }

    [JsonPropertyName("death")] public int Death { get; set; }

    [JsonPropertyName("assist")] public int Assist { get; set; }

    [JsonPropertyName("gold_earned")] public int GoldEarned { get; set; }
}

public class Meta
{
    [JsonPropertyName("first_game_created_at")]
    public DateTime FirstGameCreatedAt { get; set; }

    [JsonPropertyName("last_game_created_at")]
    public DateTime LastGameCreatedAt { get; set; }
}

public class MyData
{
    [JsonPropertyName("summoner")] public Summoner Summoner { get; set; }

    [JsonPropertyName("participant_id")] public int ParticipantId { get; set; }

    [JsonPropertyName("champion_id")] public int ChampionId { get; set; }

    [JsonPropertyName("team_key")] public string TeamKey { get; set; }

    [JsonPropertyName("position")] public string Position { get; set; }

    [JsonPropertyName("items")] public List<int> Items { get; set; }

    [JsonPropertyName("trinket_item")] public int TrinketItem { get; set; }

    [JsonPropertyName("rune")] public Rune Rune { get; set; }

    [JsonPropertyName("spells")] public List<int> Spells { get; set; }

    [JsonPropertyName("stats")] public Stats Stats { get; set; }

    [JsonPropertyName("tier_info")] public TierInfo TierInfo { get; set; }
}

public class Participant
{
    [JsonPropertyName("summoner")] public Summoner Summoner { get; set; }

    [JsonPropertyName("participant_id")] public int ParticipantId { get; set; }

    [JsonPropertyName("champion_id")] public int ChampionId { get; set; }

    [JsonPropertyName("team_key")] public string TeamKey { get; set; }

    [JsonPropertyName("position")] public string Position { get; set; }

    [JsonPropertyName("items")] public List<int> Items { get; set; }

    [JsonPropertyName("trinket_item")] public int TrinketItem { get; set; }

    [JsonPropertyName("rune")] public Rune Rune { get; set; }

    [JsonPropertyName("spells")] public List<int> Spells { get; set; }

    [JsonPropertyName("stats")] public Stats Stats { get; set; }

    [JsonPropertyName("tier_info")] public TierInfo TierInfo { get; set; }
}

public class QueueInfo
{
    [JsonPropertyName("id")] public int Id { get; set; }

    [JsonPropertyName("queue_translate")] public string QueueTranslate { get; set; }

    [JsonPropertyName("game_type")] public string GameType { get; set; }
}

public class SummonerApiResponse
{
    [JsonPropertyName("data")] public List<Datum> Data { get; set; }
}

public class Rune
{
    [JsonPropertyName("primary_page_id")] public int PrimaryPageId { get; set; }

    [JsonPropertyName("primary_rune_id")] public int PrimaryRuneId { get; set; }

    [JsonPropertyName("secondary_page_id")]
    public int SecondaryPageId { get; set; }
}

public class Stats
{
    [JsonPropertyName("champion_level")] public int ChampionLevel { get; set; }

    [JsonPropertyName("damage_self_mitigated")]
    public int DamageSelfMitigated { get; set; }

    [JsonPropertyName("damage_dealt_to_objectives")]
    public int DamageDealtToObjectives { get; set; }

    [JsonPropertyName("damage_dealt_to_turrets")]
    public int DamageDealtToTurrets { get; set; }

    [JsonPropertyName("magic_damage_dealt_player")]
    public int MagicDamageDealtPlayer { get; set; }

    [JsonPropertyName("physical_damage_taken")]
    public int PhysicalDamageTaken { get; set; }

    [JsonPropertyName("physical_damage_dealt_to_champions")]
    public int PhysicalDamageDealtToChampions { get; set; }

    [JsonPropertyName("total_damage_taken")]
    public int TotalDamageTaken { get; set; }

    [JsonPropertyName("total_damage_dealt")]
    public int TotalDamageDealt { get; set; }

    [JsonPropertyName("total_damage_dealt_to_champions")]
    public int TotalDamageDealtToChampions { get; set; }

    [JsonPropertyName("largest_critical_strike")]
    public int LargestCriticalStrike { get; set; }

    [JsonPropertyName("time_ccing_others")]
    public int TimeCcingOthers { get; set; }

    [JsonPropertyName("vision_score")] public int VisionScore { get; set; }

    [JsonPropertyName("vision_wards_bought_in_game")]
    public int VisionWardsBoughtInGame { get; set; }

    [JsonPropertyName("sight_wards_bought_in_game")]
    public int SightWardsBoughtInGame { get; set; }

    [JsonPropertyName("ward_kill")] public int WardKill { get; set; }

    [JsonPropertyName("ward_place")] public int WardPlace { get; set; }

    [JsonPropertyName("turret_kill")] public int TurretKill { get; set; }

    [JsonPropertyName("barrack_kill")] public int BarrackKill { get; set; }

    [JsonPropertyName("kill")] public int Kill { get; set; }

    [JsonPropertyName("death")] public int Death { get; set; }

    [JsonPropertyName("assist")] public int Assist { get; set; }

    [JsonPropertyName("largest_multi_kill")]
    public int LargestMultiKill { get; set; }

    [JsonPropertyName("largest_killing_spree")]
    public int LargestKillingSpree { get; set; }

    [JsonPropertyName("minion_kill")] public int MinionKill { get; set; }

    [JsonPropertyName("neutral_minion_kill_team_jungle")]
    public int? NeutralMinionKillTeamJungle { get; set; }

    [JsonPropertyName("neutral_minion_kill_enemy_jungle")]
    public int? NeutralMinionKillEnemyJungle { get; set; }

    [JsonPropertyName("neutral_minion_kill")]
    public int? NeutralMinionKill { get; set; }

    [JsonPropertyName("gold_earned")] public int GoldEarned { get; set; }

    [JsonPropertyName("total_heal")] public int TotalHeal { get; set; }

    [JsonPropertyName("result")] public string Result { get; set; }

    [JsonPropertyName("op_score")] public double OpScore { get; set; }

    [JsonPropertyName("op_score_rank")] public int OpScoreRank { get; set; }

    [JsonPropertyName("is_opscore_max_in_team")]
    public bool IsOpscoreMaxInTeam { get; set; }
}

public class Summoner
{
    [JsonPropertyName("id")] public int Id { get; set; }

    [JsonPropertyName("summoner_id")] public string SummonerId { get; set; }

    [JsonPropertyName("acct_id")] public string AcctId { get; set; }

    [JsonPropertyName("puuid")] public string Puuid { get; set; }

    [JsonPropertyName("name")] public string Name { get; set; }

    [JsonPropertyName("internal_name")] public string InternalName { get; set; }

    [JsonPropertyName("profile_image_url")]
    public string ProfileImageUrl { get; set; }

    [JsonPropertyName("level")] public int Level { get; set; }

    [JsonPropertyName("updated_at")] public DateTime? UpdatedAt { get; set; }
}

public class Team
{
    [JsonPropertyName("key")] public string Key { get; set; }

    [JsonPropertyName("game_stat")] public GameStat GameStat { get; set; }

    [JsonPropertyName("banned_champions")] public List<object> BannedChampions { get; set; }
}

public class TierInfo
{
    [JsonPropertyName("tier")] public string Tier { get; set; }

    [JsonPropertyName("division")] public int? Division { get; set; }

    [JsonPropertyName("lp")] public int? Lp { get; set; }

    [JsonPropertyName("tier_image_url")] public string TierImageUrl { get; set; }

    [JsonPropertyName("border_image_url")] public string BorderImageUrl { get; set; }
}