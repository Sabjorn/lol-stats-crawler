﻿using System.Globalization;
using System.Text;
using System.Text.Json;
using LoLCrawler.Data;

namespace LoLCrawler.Apis.BlitzGG;

public static class BlitzGGApi
{
    private const string EndPointUrl = "https://riot.iesdev.com/graphql";

    private static string GetPuuidQuery(string summonerName) => $@"
{{
  leagueProfile(summonerName:""{summonerName}"", region:JP1){{
    puuid
  }}
}}
";

    private static string GetMatchListQuery(string puuid) => $@"
{{
  matchlist(puuid:""{puuid}"", region:JP1){{
    matches{{
      playerMatch{{
        match{{
          riotMatchId
        }}
      }}
    }}
  }}
}}
";

    private static string GetMatchQuery(IEnumerable<uint> matchIds)
    {
        var stringBuilder = new StringBuilder();

        stringBuilder.AppendLine("{");

        foreach (var matchId in matchIds)
        {
            stringBuilder.AppendLine($"m{matchId}:match(matchId:{matchId}, region:JP1)");
        }

        stringBuilder.AppendLine("}");

        return stringBuilder.ToString();
    }

    public static async Task<string?> GetPuuid(string summonerName, CancellationToken ct)
    {
        var response = await HttpMethod.PostGraphQL(EndPointUrl, GetPuuidQuery(summonerName), ct);

        var puuid = response.RootElement.GetProperty("data").GetProperty("leagueProfile").GetProperty("puuid")
            .GetString();

        return puuid;
    }

    public static async Task<IReadOnlyCollection<uint>> GetMatchIds(string puuid, CancellationToken ct)
    {
        var response = await HttpMethod.PostGraphQL(EndPointUrl, GetMatchListQuery(puuid), ct);

        var matchIds = response.RootElement
            .GetProperty("data")
            .GetProperty("matchlist")
            .GetProperty("matches")
            .EnumerateArray()
            .Select(el =>
            {
                var playerMatch = el.GetProperty("playerMatch");

                if (playerMatch.ValueKind == JsonValueKind.Null) return default;

                return playerMatch
                    .GetProperty("match")
                    .GetProperty("riotMatchId")
                    .GetString();
            })
            .Where(idStr => idStr != null)
            .Select(uint.Parse)
            .ToArray();


        return matchIds;
    }

    public static async Task<IReadOnlyCollection<LoLCrawler.Data.Match>> GetMatches(IReadOnlyCollection<uint> matchIds, CancellationToken ct)
    {
        if (!matchIds.Any()) return Array.Empty<LoLCrawler.Data.Match>();
        
        var responseJson = await HttpMethod.PostGraphQL(EndPointUrl, GetMatchQuery(matchIds), ct);

        var matches = responseJson.RootElement
            .GetProperty("data")
            .EnumerateObject()
            .Select(prop => TranslateMatch(prop.Value))
            .ToArray();

        return matches;
    }

    private static LoLCrawler.Data.Match TranslateMatch(JsonElement element)
    {
        var participantElements = element.GetProperty("info").GetProperty("participants").EnumerateArray().ToArray();

        var blueParticipants = participantElements
            .Where(p => p.GetProperty("teamId").GetInt32() == 100)
            .Select(p => new Participant(
                p.GetProperty("summonerName").GetString(),
                (LoLCrawler.Data.Champion)p.GetProperty("championId").GetInt32()
            ))
            .ToArray();
        var redParticipants = participantElements
            .Where(p => p.GetProperty("teamId").GetInt32() == 200)
            .Select(p => new Participant(
                p.GetProperty("summonerName").GetString(),
                (LoLCrawler.Data.Champion)p.GetProperty("championId").GetInt32()
            ))
            .ToArray();

        var winTeam = element
            .GetProperty("info")
            .GetProperty("teams")
            .EnumerateArray()
            .Any(t => t.GetProperty("teamId").GetInt32() == 100 && t.GetProperty("win").GetBoolean())
            ? Team.Blue
            : Team.Red;

        return LoLCrawler.Data.Match.Create(blueParticipants, redParticipants, winTeam);
    }
}