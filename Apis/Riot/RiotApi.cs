﻿using System.Text.Json.Serialization;

namespace LoLCrawler.Apis.Riot;

public static class RiotApi
{
    private const string VersionApiUrl = "https://ddragon.leagueoflegends.com/api/versions.json";

    private static string GetChampionsApiUrl(string versionString) =>
        $"https://ddragon.leagueoflegends.com/cdn/{versionString}/data/en_US/champion.json";

    public static async Task<string> GetVersionString(CancellationToken ct)
    {
        var versions = await HttpMethod.GetJson<IReadOnlyCollection<string>>(VersionApiUrl, ct);

        if (versions == default) throw new ApplicationException("No versions response.");

        return versions.First();
    }

    public static async Task<IReadOnlyDictionary<int, string>> GetChampions(string versionString, CancellationToken ct)
    {
        var response = await HttpMethod.GetJson<ChampionsApiResponse>(GetChampionsApiUrl(versionString), ct);

        var dict = response.Data.ToDictionary(e => int.Parse(e.Value.Key), e => e.Value.Name);

        return dict;
    }
}