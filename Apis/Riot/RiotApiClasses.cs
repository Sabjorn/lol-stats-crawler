﻿using System.Text.Json.Serialization;

namespace LoLCrawler.Apis.Riot;
// Root myDeserializedClass = JsonSerializer.Deserialize<Root>(myJsonResponse);
    public class Champion
    {
        [JsonPropertyName("version")]
        public string Version { get; set; }

        [JsonPropertyName("id")]
        public string Id { get; set; }

        [JsonPropertyName("key")]
        public string Key { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("title")]
        public string Title { get; set; }

        [JsonPropertyName("blurb")]
        public string Blurb { get; set; }

        [JsonPropertyName("info")]
        public Info Info { get; set; }

        [JsonPropertyName("image")]
        public Image Image { get; set; }

        [JsonPropertyName("tags")]
        public List<string> Tags { get; } = new List<string>();

        [JsonPropertyName("partype")]
        public string Partype { get; set; }

        [JsonPropertyName("stats")]
        public Stats Stats { get; set; }
    }

    public class Image
    {
        [JsonPropertyName("full")]
        public string Full { get; set; }

        [JsonPropertyName("sprite")]
        public string Sprite { get; set; }

        [JsonPropertyName("group")]
        public string Group { get; set; }

        [JsonPropertyName("x")]
        public int X { get; set; }

        [JsonPropertyName("y")]
        public int Y { get; set; }

        [JsonPropertyName("w")]
        public int W { get; set; }

        [JsonPropertyName("h")]
        public int H { get; set; }
    }

    public class Info
    {
        [JsonPropertyName("attack")]
        public int Attack { get; set; }

        [JsonPropertyName("defense")]
        public int Defense { get; set; }

        [JsonPropertyName("magic")]
        public int Magic { get; set; }

        [JsonPropertyName("difficulty")]
        public int Difficulty { get; set; }
    }

    public class ChampionsApiResponse
    {
        [JsonPropertyName("type")]
        public string Type { get; set; }

        [JsonPropertyName("format")]
        public string Format { get; set; }

        [JsonPropertyName("version")]
        public string Version { get; set; }

        [JsonPropertyName("data")]
        public IDictionary<string,Champion> Data { get; set; }
    }

    public class Stats
    {
        [JsonPropertyName("hp")]
        public double Hp { get; set; }

        [JsonPropertyName("hpperlevel")]
        public double Hpperlevel { get; set; }

        [JsonPropertyName("mp")]
        public double Mp { get; set; }

        [JsonPropertyName("mpperlevel")]
        public double Mpperlevel { get; set; }

        [JsonPropertyName("movespeed")]
        public double Movespeed { get; set; }

        [JsonPropertyName("armor")]
        public double Armor { get; set; }

        [JsonPropertyName("armorperlevel")]
        public double Armorperlevel { get; set; }

        [JsonPropertyName("spellblock")]
        public double Spellblock { get; set; }

        [JsonPropertyName("spellblockperlevel")]
        public double Spellblockperlevel { get; set; }

        [JsonPropertyName("attackrange")]
        public double Attackrange { get; set; }

        [JsonPropertyName("hpregen")]
        public double Hpregen { get; set; }

        [JsonPropertyName("hpregenperlevel")]
        public double Hpregenperlevel { get; set; }

        [JsonPropertyName("mpregen")]
        public double Mpregen { get; set; }

        [JsonPropertyName("mpregenperlevel")]
        public double Mpregenperlevel { get; set; }

        [JsonPropertyName("crit")]
        public double Crit { get; set; }

        [JsonPropertyName("critperlevel")]
        public double Critperlevel { get; set; }

        [JsonPropertyName("attackdamage")]
        public double Attackdamage { get; set; }

        [JsonPropertyName("attackdamageperlevel")]
        public double Attackdamageperlevel { get; set; }

        [JsonPropertyName("attackspeedperlevel")]
        public double Attackspeedperlevel { get; set; }

        [JsonPropertyName("attackspeed")]
        public double Attackspeed { get; set; }
    }

