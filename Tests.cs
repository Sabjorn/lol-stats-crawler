﻿using LoLCrawler.Agency.Agents;
using LoLCrawler.Apis.BlitzGG;
using LoLCrawler.Apis.Riot;
using LoLCrawler.Data;

namespace LoLCrawler;

public class Tests
{
    public static void GetChampions()
    {
        HogeAsync().Wait();
    }
    private static async Task HogeAsync()
    {
        var cts = new CancellationTokenSource();
        var versionString = await RiotApi.GetVersionString(cts.Token);
        var champions = await RiotApi.GetChampions(versionString, cts.Token);
        foreach (var e in champions)
        {
            Console.WriteLine($"{e.Value} = {e.Key},");
        }
    }

    public static void Hoge()
    {
        var agency = new Agency.Agency();
        
        agency.ImportSnapshot("snapshot.json");

        using var file = File.OpenWrite("out.csv");
        using var writer = new StreamWriter(file);

        foreach (var match in agency.Matches)
        {
            var blueChampionsString = string.Join(',', match.BlueTeamChampions.Select(p => p.Champion.ToString()));
            var redChampionsString = string.Join(',', match.RedTeamChampions.Select(p => p.Champion.ToString()));
            var blueWin = match.WinTeam == Team.Blue;
            
            writer.WriteLine($"{blueChampionsString},{redChampionsString},{blueWin}");
        }
    }
}