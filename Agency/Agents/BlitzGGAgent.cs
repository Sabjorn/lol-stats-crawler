﻿using LoLCrawler.Apis.BlitzGG;
using Match = LoLCrawler.Data.Match;

namespace LoLCrawler.Agency.Agents;

public class BlitzGGAgent : IAgent
{
    public TimeSpan Interval => TimeSpan.Zero;

    public async Task<IReadOnlyCollection<Match>> Crawl(string summoner, CancellationToken ct)
    {
        var puuid = await BlitzGGApi.GetPuuid(summoner, ct);
        var matchIds = (await BlitzGGApi.GetMatchIds(puuid, ct)).Distinct().OrderBy(_ => Random.Shared.Next()).ToArray();
        var matchIdGroups = matchIds
            .GroupBy(id => id % (int)Math.Sqrt(matchIds.Length))
            .Where(g => g.Any());
        var matches = (await Task.WhenAll(matchIdGroups.Select(g => BlitzGGApi.GetMatches(g.ToArray(), ct))))
            .SelectMany(x => x)
            .ToArray();

        return matches;
    }
}