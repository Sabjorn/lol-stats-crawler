﻿using LoLCrawler.Apis.Mobalytics;
using LoLCrawler.Data;
using Match = LoLCrawler.Data.Match;
using Participant = LoLCrawler.Data.Participant;
using Team = LoLCrawler.Data.Team;

namespace LoLCrawler.Agency.Agents;

public class MobalyticsAgent : IAgent
{
    public TimeSpan Interval => TimeSpan.Zero;
    public async Task<IReadOnlyCollection<Match>> Crawl(string summoner, CancellationToken ct)
    {
        var response = await MobalyticsApi.GetSummonerInfo(summoner, ct);

        var tuple = TranslateResponse(response);

        return tuple;
    }

    private IReadOnlyCollection<Match> TranslateResponse(SummonerOverviewQueryResponse response)
    {
        var matches = response.Data.Lol.Player.MatchesHistory.Matches.Select(TranslateDatumForMatch).ToArray();

        return matches;
    }

    private Match TranslateDatumForMatch(LoLCrawler.Apis.Mobalytics.Match datum)
    {
        var blueChampions = datum.Participants.Where(p => p.Team == "BLUE")
            .Select(p => new Participant(p.SummonerName, (Champion)p.ChampionId))
            .ToArray();
        var redChampions = datum.Participants.Where(p => p.Team == "RED")
            .Select(p => new Participant(p.SummonerName, (Champion)p.ChampionId))
            .ToArray();

        var winTeam = datum.Teams.First(t => t.Result == "WON").TeamId == "BLUE" ? Team.Blue : Team.Red;

        var match = Match.Create(blueChampions, redChampions, winTeam);

        return match;
    }
}