﻿using LoLCrawler.Apis.OPGG;
using LoLCrawler.Data;
using Participant = LoLCrawler.Data.Participant;
using Team = LoLCrawler.Data.Team;

namespace LoLCrawler.Agency.Agents;

public class OPGGAgent : IAgent
{
    public TimeSpan Interval => TimeSpan.FromSeconds(1);

    public async Task<IReadOnlyCollection<Match>> Crawl(string summoner,
        CancellationToken ct)
    {
        var summonerId = await OPGGApi.GetSummonerId(summoner, ct);
        var response = await OPGGApi.GetSummonerData(summonerId, ct);

        var tuple = TranslateResponse(response);

        return tuple;
    }

    private IReadOnlyCollection<Match> TranslateResponse(
        SummonerApiResponse response)
    {
        var matches = response.Data.Select(TranslateDatumForMatch).ToArray();

        return matches;
    }

    private Match TranslateDatumForMatch(Datum datum)
    {
        var blueChampions = datum.Participants.Where(p => p.TeamKey == "BLUE")
            .Select(p => new Participant(p.Summoner.Name, (Champion)p.ChampionId))
            .ToArray();
        var redChampions = datum.Participants.Where(p => p.TeamKey == "RED")
            .Select(p => new Participant(p.Summoner.Name, (Champion)p.ChampionId))
            .ToArray();

        var winTeam = datum.Teams.First(t => t.GameStat.IsWin).Key == "BLUE" ? Team.Blue : Team.Red;

        var match = Match.Create(blueChampions, redChampions, winTeam);

        return match;
    }
}