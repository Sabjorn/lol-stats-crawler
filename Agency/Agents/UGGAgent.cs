﻿using LoLCrawler.Apis.UGG;
using LoLCrawler.Data;

namespace LoLCrawler.Agency.Agents;

public class UGGAgent : IAgent
{
    public TimeSpan Interval => TimeSpan.Zero;

    public async Task<IReadOnlyCollection<Match>> Crawl(string summoner,
        CancellationToken ct)
    {
        var response = await UGGApi.FetchMatchSummaries(summoner, ct);

        var tuple = TranslateResponse(response, summoner);

        return tuple;
    }

    private IReadOnlyCollection<Match> TranslateResponse(
        FetchMatchSummariesResponse response, string currentSummonerName)
    {
        var matches = response.Data.FetchPlayerMatchSummaries.MatchSummaries
            .Select(summary => TranslateDatumForMatch(summary, currentSummonerName)).ToArray();

        return matches;
    }

    private Match TranslateDatumForMatch(MatchSummary matchSummary, string currentSummonerName)
    {
        var blueChampions = matchSummary.TeamA
            .Select(p => new Participant(p.SummonerName, (Champion)p.ChampionId))
            .ToArray();
        var redChampions = matchSummary.TeamB
            .Select(p => new Participant(p.SummonerName, (Champion)p.ChampionId))
            .ToArray();

        var winTeam = blueChampions.Any(p => p.SummonerName == currentSummonerName) ? Team.Blue : Team.Red;

        var match = Match.Create(blueChampions, redChampions, winTeam);

        return match;
    }
}