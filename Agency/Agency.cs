﻿using System.Net;
using System.Text.Json;
using LoLCrawler.Agency.Agents;
using LoLCrawler.Data;
using LoLCrawler.Utility;

namespace LoLCrawler.Agency;

public class Agency
{
    private const int MaximumSummonersQueueSize = 1024;

    private readonly IReadOnlyCollection<IAgent> _agents = new IAgent[]
    {
        new OPGGAgent(),
        new MobalyticsAgent(),
        new UGGAgent(),
        new BlitzGGAgent()
    };

    private readonly CrawlProgress _crawlProgress = new();

    public IReadOnlyCollection<Match> Matches => _crawlProgress.Matches;
    public int NumMatches => _crawlProgress.NumMatches;

    public void EnqueueSummoner(string summoner)
    {
        lock (_crawlProgress)
        {
            _crawlProgress.Enqueue(summoner);
        }
    }

    public async Task RunAsync(CancellationToken ct)
    {
        if (_crawlProgress.QueueCount == 0)
        {
            Console.WriteLine("Summoners queue is exhausted. Generating queue out of crawled summoners...");

            var summoners = _crawlProgress.CrawledSummoners
                .OrderBy(_ => Random.Shared.Next())
                .Take(MaximumSummonersQueueSize)
                .ToArray();

            foreach (var summoner in summoners)
            {
                _crawlProgress.Enqueue(summoner);
            }
        }

        Console.WriteLine("Running all agents...");

        await Task.WhenAll(_agents.Select(agent => RunAgentAsync(agent, ct)));
    }

    private async Task RunAgentAsync(IAgent agent, CancellationToken ct)
    {
        while (!ct.IsCancellationRequested)
        {
            string summoner;
            lock (_crawlProgress)
            {
                _crawlProgress.TryDequeue(out summoner);
            }

            if (summoner == default)
            {
                await Task.Delay(100, ct);
                continue;
            }

            IReadOnlyCollection<Match> matches = Array.Empty<Match>();
            try
            {
                matches = await agent.Crawl(summoner, ct);
            }
            catch (OperationCanceledException)
            {
            }
            catch (HttpRequestException exception)
            {
                if (exception.StatusCode != HttpStatusCode.TooManyRequests) throw;
                
                Console.WriteLine($"Returned `Too many request` in {agent.GetType().Name}. Aborting...");
                break;

            }
            catch (Exception exception)
            {
                Console.WriteLine($"Something went wrong with agent {agent.GetType().Name}:\n{exception}");

                //break;
            }

            lock (_crawlProgress)
            {
                var queuedSummoners = matches.SelectMany(m =>
                        m.BlueTeamChampions.Select(p => p.SummonerName)
                            .Concat(m.RedTeamChampions.Select(p => p.SummonerName)))
                    .Distinct()
                    .OrderBy(_ => Random.Shared.Next())
                    .Take(MaximumSummonersQueueSize - _crawlProgress.QueueCount)
                    .ToArray();
                _crawlProgress.Commit(summoner, matches, queuedSummoners);
            }

            Console.WriteLine(
                $"Committed {matches.Count} matches by {agent.GetType().Name}.");

            if (ct.IsCancellationRequested) break;

            await Task.Delay(agent.Interval, ct);
        }
    }

    public string ExportProgressJson()
    {
        lock (_crawlProgress)
        {
            return _crawlProgress.ExportJson();
        }
    }

    public void ImportSnapshot(string path)
    {
        var jsonString = File.ReadAllText(path);

        lock (_crawlProgress)
        {
            _crawlProgress.Import(jsonString);
        }
    }
}

public class CrawlProgressJson
{
    public HashSet<int> MatchHashes { get; set; }
    public List<Match> Matches { get; set; }
    public HashSet<string> Summoners { get; set; }
    public Queue<string> SummonersQueue { get; set; }
}

public class CrawlProgress
{
    private readonly HashSet<int> _matchHashes = new();
    private readonly List<Match> _matches = new();
    private readonly HashSet<string> _crawledSummoners = new();
    public IReadOnlyCollection<string> CrawledSummoners => _crawledSummoners;
    private readonly Queue<string> _summonersQueue = new();

    public IReadOnlyCollection<Match> Matches => _matches;
    public int NumMatches => _matches.Count;
    public int QueueCount => _summonersQueue.Count;

    public void Commit(string targetSummoner, IReadOnlyCollection<Match> matches,
        IReadOnlyCollection<string> candidateSummoners)
    {
        _crawledSummoners.Add(targetSummoner);

        var distinctMatches = matches.Where(match => !_matchHashes.Contains(match.MatchHash)).ToArray();

        _matches.AddRange(distinctMatches);

        foreach (var distinctMatch in distinctMatches)
        {
            _matchHashes.Add(distinctMatch.MatchHash);
        }

        var distinctCandidateSummoners = candidateSummoners.Distinct()
            .Where(summoner => !_crawledSummoners.Contains(summoner) && !_summonersQueue.Contains(summoner))
            .ToArray();

        foreach (var summoner in distinctCandidateSummoners.OrderBy(_ => Random.Shared.Next()))
        {
            _summonersQueue.Enqueue(summoner);
        }
    }

    public void Enqueue(string summoner) => _summonersQueue.Enqueue(summoner);

    public bool TryDequeue(out string summoner) => _summonersQueue.TryDequeue(out summoner);

    public string ExportJson()
    {
        var json = new CrawlProgressJson
        {
            Matches = _matches,
            MatchHashes = _matchHashes,
            Summoners = _crawledSummoners,
            SummonersQueue = _summonersQueue
        };
        return JsonSerializer.Serialize(json);
    }

    public void Import(string jsonString)
    {
        _matches.Clear();
        _matchHashes.Clear();
        _crawledSummoners.Clear();
        _summonersQueue.Clear();

        var json = JsonSerializer.Deserialize<CrawlProgressJson>(jsonString);

        _matches.AddRange(json.Matches);
        _matchHashes.AddRange(json.MatchHashes);
        _crawledSummoners.AddRange(json.Summoners);
        _summonersQueue.AddRange(json.SummonersQueue);
    }
}