﻿using LoLCrawler.Data;

namespace LoLCrawler.Agency;

public interface IAgent
{
    public TimeSpan Interval { get; }
    public Task<IReadOnlyCollection<Match>> Crawl(string summonerName, CancellationToken ct);
}